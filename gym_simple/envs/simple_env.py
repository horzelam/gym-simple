import os, subprocess, time, signal
import gym
from gym import error, spaces
from gym import utils

from gym.envs.toy_text import discrete

import numpy as np
from gym.utils import seeding

import sys
from six import StringIO, b


import logging
logger = logging.getLogger(__name__)

# possible actions
UP = 0
DOWN = 1
HOLD = 2

# Scenario 1: static map
# Start is in 1st column
# P - player
# O - obstacle
# // G - goal (places to be reached)
# For simplicity - last column contains always one goal cell - G, rest is O
# Goal is not known upfront
# Player moves to rith every action - but chooses to go UP or DOWN
# Player is able to observe only limited field before him on right - e.g. 3 columns
MAPS = {
    "3x14": [
        "P     O      O",
        "  O   O   O  G",
        "  OO         O"
    ],
    "3x19": [
        "P     O        O  O",
        "  O   O   O O     G",
        "  OO        O  OOOO"
    ],
    "3x42": [
        "P     O       OOOOO   OOO           O   O",
        "  O   O   O O           O    O      O   G",
        "  OO        OOOOOOO          OO         O"
    ],
    "3x84": [
        "P     O       OOOOO               OOO           O      OOOOO       O       OOO   O",
        "  O   O   O O          OOOOOOOO     O   O  O   O   O O          OO   OOO     O   G",
        "  OO        OOOOOOOOOO                  O   O        OOOOOOO                     O"
    ],
    "unknown": [
        "P O   O       OOOOO  O                  O",
        "  O   O   O  O       OOO    OOO    O    G",
        "          O    OO           OOO   OO    O"
    ]

}
#MAP=MAPS["3x42"]
#MAP=MAPS["3x19"]
#MAP=MAPS["3x14"]
#MAP=MAPS["3x84"]

verbose = False



class SimpleEnv(gym.Env):
    #(gym.Env, utils.EzPickle):

    metadata = {'render.modes': ['human' ,'ansi', 'window']}

    def to_action_name(self, action):
        if action == UP:
            return "up"
        elif action == DOWN:
            return "down"
        elif action == HOLD:
            return "hold"
        else:
            return "INVALID"

    def __init__(self, map_name="3x19"):
        if verbose: print("SimpleEnv - init...")
        self.map_name=map_name
        desc = MAPS[map_name]
        self.desc = desc = np.asarray(desc, dtype='c')

        self.nrow, self.ncol = nrow, ncol = desc.shape
        self.reward_range = (0, 1)


        # self.viewer = None
        # self._configure_environment()

        # TODO: proper observation space
        # SHOULD BE like in cube_crash example:
        #      spaces.Box(0, 255, (FIELD_H,FIELD_W,3), dtype=np.uint8)
        #   for observation : np.zeros( (FIELD_H,FIELD_W,3), dtype=np.uint8 )
        self.observation_space = np.array
        #np.array(self.desc[:,0:3]) #spaces.Discrete(4)

        # samples: 0,1,2
        self.action_space = spaces.Discrete(3)
        #   spaces.Box(low=np.array([-self.bounds]), high=np.array([self.bounds]))

        #spaces.Tuple((spaces......
        # self.status = ....
        self.player_row = 0
        self.player_col = 0


        self.seed()
        self.viewer = None
        self.reset()
        self.episode_over = False

    # def __del__(self):
    #     ...
    #     self.env.step()
    #     os.kill(self.server_process.pid, signal.SIGINT)
    #     if self.viewer is not None:
    #         os.kill(self.viewer.pid, signal.SIGKILL)

    # def _configure_environment(self):
    #     """
    #     Provides a chance for subclasses to override this method and supply
    #     a different server configuration. By default, we initialize one
    #     offense agent against no defenders.
    #     """
    #     ....


    # def _start_viewer(self):
    #     ...

    def _seed(self, seed=None):
       self.np_random, seed = seeding.np_random(seed)
       return [seed]

    def _step(self, action):
        if verbose: print("SimpleEnv - reqested action: {}".format(self.to_action_name(action)))
        # self._take_action(action)
        # self.status = self.env.step()
        # ....

        assert self.action_space.contains(action)

        # clean the previous player position
        self.desc[self.player_row, self.player_col]=' '

        self.player_col += 1
        if action == UP:
            self.player_row = (self.player_row - 1) if self.player_row > 0 else 0
        elif action == DOWN:
            self.player_row = (self.player_row + 1) if self.player_row < self.nrow-1 else self.nrow-1



        letter = self.desc[self.player_row, self.player_col]
        #print("SimpleEnv  -- letter in new cell: {}".format(letter))

        self.episode_over = True if letter in b'GO' else False
        reward = True if letter in b'G' else False

        # put player in new position
        new_player_letter = 'P'
        if  self.episode_over and reward :
            new_player_letter = 'F'
        elif self.episode_over and not reward:
            new_player_letter = 'X'

        self.desc[self.player_row, self.player_col] = new_player_letter


        #print("SimpleEnv  -- episode_over : {} because cell is {}".format(episode_over, self.desc[self.player_row][self.player_col] ))

        ob = self._get_observation()
        return ob, reward, self.episode_over, {}

    def _take_action(self, action):
        """ Converts the action space into an HFO action. """
        if verbose: print("SimpleEnv take_action..")

    def _get_observation(self):
        """Get the observation."""
        observation = np.array(self.desc[:,self.player_col:self.player_col+4])
        #print("Observation is : \n{}".format(observation))
        return observation


    def _reset(self):
        if verbose: print("SimpleEnv - reset..")
        # TODO: make it better - reading position of 'S' from table:

        desc = MAPS[self.map_name]
        self.desc = desc = np.asarray(desc, dtype='c')

        self.player_row = 0
        self.player_col = 0
        return self._get_observation()

    def _render(self, mode='human', close=False):
        print("SimpleEnv - RENDER..")
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        outfile = StringIO() if mode == 'ansi' else sys.stdout
        desc = self.desc.tolist()
        desc = [[c.decode('utf-8') for c in line] for line in desc]

        # get the player position and colorize it
        row = self.player_row
        col = self.player_col

        if desc[row][col] == 'P':
            color = 'green'
        elif desc[row][col] == 'F':
            color = 'blue'
        else:
            color = 'red'
        desc[row][col] = utils.colorize(desc[row][col], color, highlight=True)

        self.colorize_final_goal(desc)

        outfile.write("\n".join(''.join(line) for line in desc) + "\n")

        print("Mode")
        print(mode)
        if mode == 'human' or mode == "ansi":
            return outfile
        elif mode == 'window':
            from gym.envs.classic_control import rendering
            if self.viewer is None:
                self.viewer = rendering.SimpleImageViewer()
            self.viewer.imshow(self.convertToRgbArray(self._get_observation()))
            return self.viewer.isopen
        else:
            return outfile


        #return [2]

    def colorize_final_goal(self, desc):
        # TODO: remove this hardcoded colorization of goal
        final_column = len(desc[0])-1
        desc[0][final_column] = utils.colorize(desc[0][final_column], "green", highlight=True)
        desc[1][final_column] = utils.colorize(desc[1][final_column], "green", highlight=True)
        desc[2][final_column] = utils.colorize(desc[2][final_column], "green", highlight=True)


    color_black = np.array((0, 0, 0)).astype('float32')
    color_blue = np.array((40, 40, 255)).astype('float32')
    color_white = np.array((255, 255, 255)).astype('float32')
    color_green = np.array((0, 255, 0)).astype('float32')
    color_light_green= np.array((40, 40, 255)).astype('float32')
    color_red = np.array((255, 20, 40)).astype('float32')
    color_yellow = np.array((255, 255, 0)).astype('float32')

    def convertToRgbArray(self, obs):
        """
        Converts to RGB array the Observation
        :param desc: like flat version of np.asarray(desc, dtype='c')
        :return: array with 3 rgb values - size:  Height: 300, Width: 400
        """

        scale = 100
        view = np.zeros((3*scale, 4*scale, 3), dtype=np.uint8)
        # for pos in range(1,100):
        #     view[:, pos*scale:(pos+1)*scale, :] =  np.array((0, 0, 0)).astype('float32')
        view[:, :, :] = self.color_black

        #view[self.player_row*scale:self.player_row*scale + 100, self.player_col*scale:self.player_col*scale + 100, :] = self.color_green

        lineNr=0
        for line in obs:
            colNr=0
            for cellValue in line:
                if cellValue in b'O':
                    self.paintObstacle(view, lineNr, colNr, scale, self.color_yellow)
                elif cellValue in b'G':
                    self.paintObstacle(view, lineNr, colNr, scale, self.color_white)
                elif cellValue in b'P':
                    self.paintPlayer(view, lineNr, colNr, scale, self.color_green)
                elif cellValue in b'F':
                    self.paintObstacle(view, lineNr, colNr, scale, self.color_light_green)
                    self.paintPlayer(view, lineNr, colNr, scale, self.color_green)
                elif cellValue in b'X':
                    self.paintObstacle(view, lineNr, colNr, scale, self.color_red)
                    self.paintPlayer(view, lineNr, colNr, scale, self.color_green)

                colNr += 1
            lineNr += 1
        return view

    def paintObstacle(self, view, lineNr, colNr, scale, color):
        round = scale // 10
        for pixel_pos in range(scale):
            view[lineNr * scale + (round - min(round,pixel_pos) + max(0,pixel_pos - (scale-round)) )  :  (lineNr +1)* scale - (round - min(round,pixel_pos)) - max(0,pixel_pos - (scale-round)),
            colNr * scale + pixel_pos :colNr* scale + pixel_pos +1, :] = color
        # view[lineNr * scale:lineNr * scale + 100,
        # colNr * scale:colNr * scale + 100, :] = color

    def paintPlayer(self, view, lineNr, colNr, scale, color):
        triangle_range = 2 * scale // 3
        round = scale // 10
        for pixel_pos in range(triangle_range):
            dyn_color = np.array((0, 70 + 180*(pixel_pos / triangle_range), 0)).astype('float32')
            view[lineNr * scale + round + pixel_pos:lineNr * scale + (scale - round) - pixel_pos,
            colNr * scale + 2*round + pixel_pos:colNr * scale + 2*round + pixel_pos + 1, :] = dyn_color
        #view[lineNr * scale + 20:lineNr * scale + 80,
        #colNr * scale + 20 :colNr * scale + 80, :] = color
