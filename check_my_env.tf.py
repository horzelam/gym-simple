##############################################################
# Runs Simple-v0 game with TensorFlow
##############################################################
# Before running make sure to have activated :
# source active universe
##############################################################
import gym
#import universe  # register the universe environments
#from universe import wrappers
import numpy as np
import results
from gym_simple.envs.simple_env import SimpleEnv

import tensorflow as tf

import conversion as conv


env = gym.make('Simple-v0')


tf.reset_default_graph()


#These lines establish the feed-forward part of the network used to choose actions


# TODO:
# Decide about model
#
# We have 3 possible Player places - for each combination of Obstacles in next 3 column is: 512 => so total combinations 3x512
# In each case Player can take one of 3 actions : hold, up, down
#
# In Frozen lake game , observation was player position in 4x4 - only 16 possible states to deal with it - returned as just single integer number
# here we have 3 player states (so it could be number 0..2) + 512 states of Obstacles (so it could be number 0..512)

# TO CHECK - it could be just array of 1 row with 12 cells (3x4) with possible 1 or 0 values

inputs1 = tf.placeholder(shape=[1,12],dtype=tf.float32)
W = tf.Variable(tf.random_uniform([12,3],0,0.01))
Qout = tf.matmul(inputs1,W)
predict = tf.argmax(Qout,1)

#Below we obtain the loss by taking the sum of squares difference between the target and prediction Q values.
nextQ = tf.placeholder(shape=[1,3],dtype=tf.float32)
loss = tf.reduce_sum(tf.square(nextQ - Qout))
trainer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
updateModel = trainer.minimize(loss)


init = tf.initialize_all_variables()


# Set learning parameters
discountVal = .99
e = 0.1

#create lists to contain total rewards and steps per episode
jList = []
rList = []

# Make sense to do 2000
#num_episodes = 2000

num_episodes = 500
verbose=False


if verbose:
    print("Action space:")
    print(env.action_space) # Discrete:2
    print("Observation space:")
    print(env.observation_space) # None

with tf.Session() as sess:
    sess.run(init)
    for i_episode in range(num_episodes):

        rAll = 0

        if verbose:
            print("------------------------------------------------")
            print("--------Episode nr {}---------------------------".format(i_episode+1))
            print("------------------------------------------------")
        observation = env.reset()
        if verbose: env.render()
        done = False

        step = 0
        while step < 100:
            step += 1
            if verbose: print("-------------------------\nStep nr {}".format(step))

            # Choose an action by greedily (with e chance of random action) from the Q-network
            inputConverted=conv.convertObservation(observation)
            if verbose: print("Input is : {}".format(inputConverted))
            a, allQ = sess.run([predict, Qout], feed_dict={inputs1: inputConverted})
            if np.random.rand(1) < e:
                a[0] = env.action_space.sample()

            # Get new state and reward from environment
            new_observation, reward, done, _ = env.step(a[0])

            if verbose: env.render()
            #print(new_observation)

            # Obtain the Q' values by feeding the new state through our network
            Q1 = sess.run(Qout, feed_dict={inputs1: conv.convertObservation(new_observation)})
            # Obtain maxQ' and set our target value for chosen action.
            maxQ1 = np.max(Q1)
            targetQ = allQ
            targetQ[0, a[0]] = reward + discountVal * maxQ1
            # Train our network using target and predicted Q values
            _, W1 = sess.run([updateModel, W], feed_dict={inputs1: conv.convertObservation(observation), nextQ: targetQ})

            rAll += reward
            observation = new_observation
            if done == True:
                # Reduce chance of random action as we train the model.
                e = 1. / ((i_episode / 50) + 10)
                if verbose: print("----------------Episode nr {}--------------------------".format(i_episode))
                if verbose: print("------------------------------------------------")
                if verbose: print("------Episode finished after {} timesteps".format(step + 1))
                if verbose: print("------------------------------------------------")
                if reward == 1:
                    if verbose:  print("------------ Episode nr {} - WIN ! -------------".format(i_episode))
                break
        jList.append(step)
        rList.append(rAll)
print("Percent of succesful episodes: " + str(100*sum(rList)/num_episodes) + "%")

###########################################################################
# Percent of succesful episodes: 1 - 52.0%
###########################################################################

results.plot(rList)