# Simple gym env
This is simple gym env I created to try Q-learning and TensorFlow  

It's horizontally scalling game with Player which needs to avoid obstacles to reach the goal.

Agent sees only the view 3x4 - not the whole board.
 

# Visualization
In qtable/TF examples it does ~1000 episodes but renders just last episodes.  
Render renders to sysout and additionally to "window".   

The observation is only **3x4 view** with current player   
position on the whole long game board. 
The whole board in simplest form is **3x19**,   
but with more episodes the model was check with longer boards **3x42*  
 
**Green triangle** is Player  
**Yellow blocks** are obstacles.  
**White block** is the final Goal.

See game screenshoot at the moment just before finishing:    
![game screenshot](game_screenshot.png)

# Tensor flow solution code
* with 4096 NN inputs [check_my_env.tf.more_inputs.py](https://bitbucket.org/horzelam/gym-simple/src/master/check_my_env.tf.more_inputs.py)
* with 12 NN inputs [check_my_env.tf.new.py](https://bitbucket.org/horzelam/gym-simple/src/master/check_my_env.tf.new.py)

# Q-learning solution code
* [check_my_env.qtable.py](https://bitbucket.org/horzelam/gym-simple/src/master/check_my_env.qtable.py)

# Game code
The code which models the game  is in file:
* [gym_simple/envs/simple_env.py](https://bitbucket.org/horzelam/gym-simple/src/master/gym_simple/envs/simple_env.py)

# Installation
Prerequisites:
* anaconda with gym installed (notes from installation: https://docs.google.com/document/d/1g2X50X5Z_OjJz45aYbEqLTmVoZwDp_dmVUSd3F8FMuM/edit)

```bash
# activate conda env in which you want to compile it
source activate universe

# enter the project and build it
cd gym-simple
pip install -e .
```

# Testing:
```bash
# activate conda env in which you want to compile it
source activate universe

# enter the project and run test
cd gym-simple

# To run just smoke test
python check_my_env.smoketest.py

# To run Q-table based solution
python check_my_env.qtable.py

# To run TensorFlow NN based solution
python check_my_env.tf.more_inputs.py

#python test_my_env.py

```




# TODO:
1. DONE - define the game representation - table 3 rows x XX columns with static obstacles
2. DONE - define the possible actions
3. DONE - implement render
4. DONE - implement progress - each step move to right
5. DONE - implement handling an action
6. DONE - implement end of game - reaching end of table
7. DONE - Define the observation (view on next 3 columns with player position) + return it
8. DONE - Solve game using tensor flow - reacting on observation
9. DONE - Try with different model (more inputs etc. , different params)
10. DONE - output progress with simple animation
11. DONE - Get the loss - and stop training in moment when it's stopping to improve 
11. save/load model - try the model on different bigger boads
12. logger (with switch DEBUG/INFO)
13. dynamic game board generator
14. why env.action_space.sample() returns always the same seqence despite seed is defined ?

Inspiration for Loss calculations from observations - see https://minpy.readthedocs.io/en/latest/tutorial/rl_policy_gradient_tutorial/rl_policy_gradient.html

# STATE:
Recent solution is in check_my_env.tf.more_inputs based on TensorFlow


# Links
To create new gym eng - See: https://stackoverflow.com/questions/45068568/is-it-possible-to-create-a-new-gym-environment-in-openai

