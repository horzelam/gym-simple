##############################################################
# Runs Simple-v0 game with TensorFlow
##############################################################
# Before running make sure to have activated :
# source active universe
##############################################################
import gym
#import universe  # register the universe environments
#from universe import wrappers
import numpy as np
import results
from gym_simple.envs.simple_env import SimpleEnv

import tensorflow as tf

import conversion as conv


env = gym.make('Simple-v0')


tf.reset_default_graph()


#
# We have 3 possible Player places - for each combination of Obstacles in next 3 column is: 512 => so total combinations 3x512
# In each case Player can take one of 3 actions : hold, up, down
#
# Observation as 3x4 array with P,O,G characters is translated to 12 cells array with 0 and 1 values
# This array (1x12) is sent to NN Input with 12 inputs


inputs1 = tf.placeholder(shape=[1,12],dtype=tf.float32)
W = tf.Variable(tf.random_uniform([12,3],0,0.01))
Qout = tf.matmul(inputs1,W)
predict = tf.argmax(Qout,1)

#Below we obtain the loss by taking the sum of squares difference between the target and prediction Q values.
nextQ = tf.placeholder(shape=[1,3],dtype=tf.float32)
loss = tf.reduce_sum(tf.square(nextQ - Qout))
trainer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
updateModel = trainer.minimize(loss)


init = tf.initialize_all_variables()


# Set learning parameters
discountVal = .99
e = 0.1

#create lists to contain total rewards and steps per episode
jList = []
rList = []

#num_episodes = 2000

num_episodes = 500
verbose=False


if verbose:
    print("Action space:")
    print(env.action_space) # Discrete:2
    print("Observation space:")
    print(env.observation_space) # None


def discount(step):
    #PRIMITIVE WORKAROUND to make this discount growing TODO: make it better
    return discountVal
    #return discountVal * ( (13.0 - step)/14.0 )


if __name__ == '__main__':
    with tf.Session() as sess:
        sess.run(init)
        for i_episode in range(num_episodes):

            rAll = 0

            if verbose:
                print("------------------------------------------------")
                print("--------Episode nr {}---------------------------".format(i_episode+1))
                print("------------------------------------------------")
            observation = env.reset()
            observationConverted = conv.convertObservation(observation)
            if verbose: env.render()
            done = False

            step = 0
            while step < 100:
                step += 1
                if verbose: print("-------------------------\nStep nr {}".format(step))

                # converts to array of 1 x 12 FLOAT: values [[1. ,0. ,0. ,0. ,0. ,0. ,0. ,0. ,0. ,1. , 0., 0.]]

                if verbose: print("Input is : {}".format(observationConverted))


                # Choose an action by greedily (with e chance of random action) from the Q-network
                a, allQ = sess.run([predict, Qout], feed_dict={inputs1: observationConverted})


                if np.random.rand(1) < e:
                    a[0] = env.action_space.sample()

                # Get new state and reward from environment
                new_observation, reward, done, _ = env.step(a[0])

                new_observationConverted = conv.convertObservation(new_observation)

                # Override it WHEN FAILS - this is very specific state - with ZEROs
                if done == True and reward == 0:
                    new_observationConverted = np.zeros(shape=[1,12])

                if verbose: env.render()
                #print(new_observation)

                # Obtain the Q' values by feeding the new state through our network
                Q1 = sess.run(Qout, feed_dict={inputs1: new_observationConverted})
                # Obtain maxQ' and set our target value for chosen action.
                maxQ1 = np.max(Q1)
                targetQ = allQ

                # Note (to review):
                # Previous State is not considered : allQ[0,a[]]
                # because it's currently deterministic system,
                # where learning rate = 1 -> that's why prev value is ignored

                targetQ[0, a[0]] = reward + discount(step) * maxQ1
                # Train our network using target and predicted Q values
                _, W1 = sess.run([updateModel, W], feed_dict={inputs1: observationConverted, nextQ: targetQ})


                rAll += reward
                observation = new_observation
                observationConverted = new_observationConverted
                if done == True:
                    # Reduce chance of random action as we train the model.
                    e = 1. / ((i_episode / 50) + 10)
                    if verbose: print("----------------Episode nr {}--------------------------".format(i_episode))
                    if verbose: print("------------------------------------------------")
                    if verbose: print("------Episode finished after {} timesteps".format(step + 1))
                    if verbose: print("------------------------------------------------")
                    if reward == 1:
                        if verbose: print("------------ Episode nr {} - WIN ! -------------".format(i_episode))
                    break
            jList.append(step)
            rList.append(rAll)
    print("Percent of succesful episodes: " + str(100*sum(rList)/num_episodes) + "%")

    ###########################################################################
    # Percent of succesful episodes: 1 .. 78.2%
    ###########################################################################
    results.plot(rList)