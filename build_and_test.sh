#!/usr/bin/env bash

# Remember to activate universe conda env
# > source activate universe
#
# To list env:
# conda-env list


pip install -e .

# simple run - without solution, just to run episodes
python check_my_env.smoketest.py

# attempt to solve it with TensorFlow
#python check_my_env.tf.py

#python check_my_env.tf.new.py

# Q-learning table:
#python check_my_env.tf.new.py
