##############################################################
# Runs Simple-v0 game with TensorFlow
##############################################################
# Before running make sure to have activated :
# source active universe
##############################################################
import time

import gym
#import universe  # register the universe environments
#from universe import wrappers
import numpy as np
from gym_simple.envs.simple_env import SimpleEnv

import tensorflow as tf

import conversion as conv
import matplotlib.pyplot as plt
import results

env = gym.make('Simple-v0')


tf.reset_default_graph()


#
# We have 3 possible Player places
# In each case Player can take one of 3 actions : hold, up, down
#
# We have 3 player states (so it could be number 0..2) + 512 states of Obstacles (so it could be number 0..512)
#
# NN output is 3 cells array with wages for each of Action : UP, DOWN, HOLD
#
# Observation : array of 1 row with 12 cells (3x4) with P,O,G letters
# What matters is just P and O -> they are translated to 1 and then to some integer number representing state
# This number is translated to NN Input with 4096 cells: [0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,...] with just 1 bit (input) set to "1" out of 2pow12 (4096) all NN inputs
# In other words we have 2pow12 possible states (not all will be used but it doesn't matter)
# Exceptional case is LOSING the game -> it's translated to NN Input with all bits set to Zeros

# all posible inputs + all representations for it in 'allInputs'
allInputsAmount=pow(2, 12)
allInputs = np.identity(allInputsAmount)

# input - array of 4096 bit inputs:
inputs1 = tf.placeholder(shape=[1, allInputsAmount], dtype=tf.float32)

# each state has 3 outcomes with weights
# so we have 4096 x 3 matrix with weights
weights = tf.Variable(tf.random_uniform([allInputsAmount, 3], 0, 0.01))

# dot product of inputs and weights:
Qout = tf.matmul(inputs1, weights)
predict = tf.argmax(Qout,1)

#Below we obtain the loss by taking the sum of squares difference between the target and prediction Q values.
nextQ = tf.placeholder(shape=[1,3],dtype=tf.float32)
loss = tf.reduce_sum(tf.square(nextQ - Qout))
trainer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
updateModel = trainer.minimize(loss)


init = tf.initialize_all_variables()


# Set learning parameters
discountVal = .99
e = 0.1

#create lists to contain total rewards and steps per episode
jList = []
rList = []
lossList=[]

#num_episodes = 2000
#num_episodes = 5000 # -->  94.62%
num_episodes = 2000
verbose=False

# Introduced to stop when change in loss is lower than defined threshold:
# - how many last episodes are taken to calculate the min-max
lastNEpisodesToCheckForLossStop=10
lossDiffStopThreshold=0.05
minAmountOfSuccessEpisodes=40

if verbose:
    print("Action space:")
    print(env.action_space) # Discrete:2
    print("Observation space:")
    print(env.observation_space) # None


def discount(step):
    #PRIMITIVE WORKAROUND to make this discount growing TODO: make it better
    return discountVal
    #return discountVal * ( (13.0 - step)/14.0 )

def lossDiffBelowThreshold(lossList, rangeSize, lossDiffMinThreshold):
    lossListLastIndex = len(lossList) - 1
    min1 = min(lossList[lossListLastIndex - rangeSize:lossListLastIndex])
    max1 = max(lossList[lossListLastIndex - rangeSize:lossListLastIndex])
    return abs(max1 - min1) < lossDiffMinThreshold

def successRateProcent(rList, num_episodes):
    return 100 * sum(rList) / num_episodes



def playFinalDemo(envToPlay):
    '''
    JUST TO SHOW SOME DEMO AT THE END - assuming that Agent is well trained
    '''
    print("STARTING DEMO...")
    observation = envToPlay.reset()
    observationConverted = conv.convertObservationToBinaryInputs(observation, allInputs)
    envToPlay.render(mode='window')
    step = 0
    while step < 100:
        step += 1

        # Choose an action
        a, allQ = sess.run([predict, Qout], feed_dict={inputs1: observationConverted})

        # Get new state and reward from environment
        new_observation, reward, done, _ = envToPlay.step(a[0])
        envToPlay.render(mode='window')
        if done == True :

            print("FINISHED DEMO with result: {}".format(reward))
            return

        new_observationConverted = conv.convertObservationToBinaryInputs(new_observation, allInputs)
        # Override it WHEN FAILS - this is very specific state - with ZEROs
        time.sleep(0.1)
        observationConverted = new_observationConverted

def playWithOtherEnv():
    from gym.envs.registration import register
    env2 = register(
        id='Simple-2-v0',
        entry_point='gym_simple.envs:SimpleEnv',
        kwargs={'map_name': '3x42'},
    )
    env2 = gym.make('Simple-2-v0')
    playFinalDemo(env2)


if __name__ == '__main__':

    with tf.Session() as sess:
        sess.run(init)
        for i_episode in range(num_episodes):

            rAll = 0
            episodeLoss = 0

            if verbose:
                print("------------------------------------------------")
                print("--------Episode nr {}---------------------------".format(i_episode+1))
                print("------------------------------------------------")
            observation = env.reset()
            observationConverted = conv.convertObservationToBinaryInputs(observation, allInputs)
            if verbose: env.render()
            done = False

            step = 0
            while step < 100:
                step += 1
                if verbose: print("-------------------------\nStep nr {}".format(step))

                # converts to array of 1 x 12 FLOAT: values [[1. ,0. ,0. ,0. ,0. ,0. ,0. ,0. ,0. ,1. , 0., 0.]]

                if verbose: print("Input is : {}".format(observationConverted))


                # Choose an action by greedily (with e chance of random action) from the Q-network
                a, allQ = sess.run([predict, Qout], feed_dict={inputs1: observationConverted})


                if np.random.rand(1) < e:
                    a[0] = env.action_space.sample()

                # Get new state and reward from environment
                new_observation, reward, done, _ = env.step(a[0])

                new_observationConverted = conv.convertObservationToBinaryInputs(new_observation, allInputs)

                # Override it WHEN FAILS - this is very specific state - with ZEROs
                if done == True and reward == 0:
                    new_observationConverted = np.zeros(shape=[1, allInputsAmount])


                # TO SHOW SOME DEMO AT THE END:
                # if i_episode > num_episodes - 4:
                #     env.render(mode='window')
                #     time.sleep(0.1)

                #print(new_observation)

                # Obtain the Q' values by feeding the new state through our network
                Q1 = sess.run(Qout, feed_dict={inputs1: new_observationConverted})
                # Obtain maxQ' and set our target value for chosen action.
                maxQ1 = np.max(Q1)
                targetQ = allQ

                # Note (to review):
                # Previous State is not considered : allQ[0,a[]]
                # because it's currently deterministic system,
                # where learning rate = 1 -> that's why prev value is ignored

                targetQ[0, a[0]] = reward + discount(step) * maxQ1
                # Train our network using target and predicted Q values
                _, new_weights , loss1 = sess.run([updateModel, weights, loss], feed_dict={inputs1: observationConverted, nextQ: targetQ})

                #print("loss:{}".format(loss1))
                episodeLoss+=loss1


                rAll += reward
                observation = new_observation
                observationConverted = new_observationConverted
                if done == True:
                    # Reduce chance of random action as we train the model.
                    e = 1. / ((i_episode / 50) + 10)
                    if verbose: print("----------------Episode nr {}--------------------------".format(i_episode))
                    if verbose: print("------------------------------------------------")
                    if verbose: print("------Episode finished after {} timesteps".format(step + 1))
                    if verbose: print("------------------------------------------------")
                    if reward == 1:
                        if verbose: print("------------ Episode nr {} - WIN ! -------------".format(i_episode))
                    break
            jList.append(step)
            rList.append(rAll)
            lossList.append(episodeLoss/step)

            # To stop when diff in LOSS is small enough AND the succes rate is good enough
            if i_episode > lastNEpisodesToCheckForLossStop and sum(rList)  > minAmountOfSuccessEpisodes \
                    and lossDiffBelowThreshold(lossList, lastNEpisodesToCheckForLossStop, lossDiffStopThreshold):
                print("Good enough to stop learning at episode: {}".format(i_episode))
                break;
        # TO SHOW SOME DEMO AT THE END:
        print("Playing 1 episode with the same board as used in learning:")
        playFinalDemo(env)
        print("Playing 1 episode with differnt board, but with similar patterns:")
        playWithOtherEnv()

        print("Percent of succesful episodes: {}%" .format(successRateProcent(rList, num_episodes)))

    #print("Achieved Loss max-min diff below {} in last {} episodes".format( lossDiffStopThreshold, lastNEpisodesToCheckForLossStop, ))

    ###########################################################################
    # Percent of succesful episodes: ~78.4% .. 89.8%
    ###########################################################################
    results.plot(rList)



    plt.plot(lossList, 'ro', color='g', ms=1.5)
    plt.show()
