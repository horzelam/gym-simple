##############################################################
# Runs the game with random steps - just to see how it works
##############################################################
# Before running make sure to have activated :
# source active universe
##############################################################
import time

import gym
#import universe  # register the universe environments
#from universe import wrappers

from gym_simple.envs.simple_env import SimpleEnv

env = gym.make('Simple-v0')
#env = wrappers.experimental.SafeActionSpace(env)
#env.configure(remotes=1)




print("Action space:")
print(env.action_space) # Discrete:2
print("Observation space:")
print(env.observation_space) # None

for i_episode in range(10):

    print("------------------------------------------------")
    print("--------Episode nr {}---------------------------".format(i_episode))
    print("------------------------------------------------")
    observation = env.reset()
    print(observation)
    for step in range(100):
        print("-------------------------\nStep nr {}".format(step))
        time.sleep(0.1)
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        #print(observation)
        env.render(mode='window')
        if reward:
            exit()
        if done:
            time.sleep(0.5)
            print("------------------------------------------------")
            print("------Episode finished after {} timesteps".format(step+1))
            print("------------------------------------------------")
            break

