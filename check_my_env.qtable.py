##############################################################
# Runs Simple-v0 game with TensorFlow
##############################################################
# Before running make sure to have activated :
# source active universe
##############################################################
import time

import gym
#import universe  # register the universe environments
#from universe import wrappers
import numpy as np
from gym_simple.envs.simple_env import SimpleEnv

import tensorflow as tf

import conversion as conv
import results

env = gym.make('Simple-v0')


# Set learning parameters
discountVal = .99
e = 0.1
lr = .8

#create lists to contain total rewards and steps per episode
jList = []
rList = []

# Make sense to do 2000
#num_episodes = 2000

num_episodes = 2000
verbose=False

# IF episodes 2000:
# successful episodes = 8% when Action is totally random


if verbose:
    print("Action space:")
    print(env.action_space) # Discrete:2
    print("Observation space:")
    print(env.observation_space) # None
    #print(" size: {}".format(env.observation_space.n))  # None

# Initialize table with all zeros
# TODO: correct when observation space is correct
# observation_space = env.observation_space.n
# now hardcoded 12 digit binary number representing all possibilites

observation_space = pow(2, 12) + 1

FAILED_OB_NUM = pow(2,12) # the last field in Q

Q = np.zeros([observation_space, env.action_space.n])

print("Initialized Q table with params: {}, {}".format(observation_space, env.action_space.n))

# Note:
# As long as player is playing - observationNum won't be 0

for i_episode in range(num_episodes):

    rAll = 0

    if verbose:
        print("------------------------------------------------")
        print("--------START Episode nr {}---------------------------".format(i_episode+1))
        print("------------------------------------------------")
    observation = env.reset()

    observationNum = conv.convertObservationToNum(observation)
    if verbose: env.render()
    done = False

    step = 0
    while step < 100:
        step += 1
        if verbose: print("-------------------------\nStep nr {}".format(step))


        # Choose an action by greedily (with e chance of random action) from the Q-network:
        # - create array as copy of row from Q for given observation
        # - updating all Action columns with some random values (which decreases over episodes)
        possibleActionsWithNoise = Q[observationNum, :] + np.random.randn(1, env.action_space.n) * (1. / (i_episode + 1))
        # - get the best action:
        action = np.argmax(possibleActionsWithNoise)

        new_observation, reward, done, _ = env.step(action)
        new_observationNum = conv.convertObservationToNum(new_observation)

        # Note:
        #   for WIN - DONE && REWARD:1 -> the observationNum should be 0
        #   for LOST - DONE && REWARD:0 -> the observationNum should be pow(2, 12) - last field in our Q table
        if done == True and reward == 0:
            new_observationNum = FAILED_OB_NUM

        # Update Q-Table
        Q[observationNum, action] = Q[observationNum, action] + lr * (reward + discountVal * np.max(Q[new_observationNum, :]) - Q[observationNum, action])


        if verbose: env.render()

        # TO SHOW SOME DEMO AT THE END:
        if i_episode > num_episodes-2 :
            env.render(mode='window')
            time.sleep(0.1)

        rAll += reward
        observation = new_observation
        observationNum = new_observationNum
        if done == True:
            # Reduce chance of random action as we train the model.
            e = 1. / ((i_episode / 50) + 10)
            if verbose: print("----------------DONE Episode nr {}--------------------------".format(i_episode))
            if verbose: print("------------------------------------------------")
            if verbose: print("------Episode finished after {} timesteps".format(step + 1))
            if verbose: print("------------------------------------------------")
            if reward == 1:
                if verbose: print("------------ Episode nr {} - WIN ! -------------".format(i_episode))
            break
    jList.append(step)
    rList.append(rAll)
print("Percent of succesful episodes: " + str(100 * sum(rList) / num_episodes) + "%")

print("Rows in Q table with some values:")
print(Q[np.sum(Q,axis=1) > 0.7 ] )

###########################################################################
# Percent of succesful episodes: 1 - 82.0%
###########################################################################
results.plot(rList)