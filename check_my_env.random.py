##############################################################
# Runs Simple-v0 game with random steps + measuring success
##############################################################
# Before running make sure to have activated :
# source active universe
##############################################################
import gym
#import universe  # register the universe environments
#from universe import wrappers
import numpy as np
from gym_simple.envs.simple_env import SimpleEnv

import conversion as conv

import results

env = gym.make('Simple-v0')


print("Action space:")
print(env.action_space) # Discrete:2
print("Observation space:")
print(env.observation_space) # None



#create lists to contain total rewards and steps per episode
jList = []
rList = []

# Make sense to do 2000
#num_episodes = 2000

num_episodes = 2000
verbose=False

# IF episodes 2000:
# successful episodes = 8% when Action is totally random

for i_episode in range(num_episodes):

    rAll = 0

    if verbose:
        print("------------------------------------------------")
        print("--------Episode nr {}---------------------------".format(i_episode+1))
        print("------------------------------------------------")
    observation = env.reset()
    if verbose: env.render()
    done = False
    step = 0
    while step < 100:
        step += 1
        if verbose: print("-------------------------\nStep nr {}".format(step))

        # Choose an action by greedily (with e chance of random action) from the Q-network
        action = env.action_space.sample()
        # Get new state and reward from environment
        new_observation, reward, done, _ = env.step(action)

        if verbose: env.render()

        rAll += reward
        observation = new_observation
        if done == True:
            # Reduce chance of random action as we train the model.
            e = 1. / ((i_episode / 50) + 10)
            if verbose: print("----------------Episode nr {}--------------------------".format(i_episode))
            if verbose: print("------------------------------------------------")
            if verbose: print("------Episode finished after {} timesteps".format(step + 1))
            if verbose: print("------------------------------------------------")
            if reward == 1:
                if verbose: print("------------ Episode nr {} - WIN ! -------------".format(i_episode))
            break
    jList.append(step)
    rList.append(rAll)
print("Percent of succesful episodes: " + str(100*sum(rList)/num_episodes) + "%")

###########################################################################
# Percent of succesful episodes: ~2.3%
###########################################################################
results.plot(rList)
