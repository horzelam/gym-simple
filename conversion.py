import numpy as np


def convertObsLine(xi:list):
    # print("..conv line: {}".format(xi))
    convertedLine = [mapToFloat(elem) for elem in xi]
    padded = np.zeros(shape=[4])
    padded[:len(convertedLine)] = convertedLine
    # print("..padded floats: {}".format(padded))
    return np.array(padded)

def convertObservation(observation):
    """
    Converts observation to format used in TF model
    :param observation: observation as 3 rows x (max 4) columns array with lists where each cell contain letter: P,O,G; If observation has less columns -it's probably end of game so filled with null
    :return: array of 12 float values
    """
    observationAsFloats = np.array([convertObsLine(xi) for xi in observation]) #np.array(observation)

    # print("observationAsFloats :")
    # print(observationAsFloats)

    flatObservation = [item for sublist in observationAsFloats for item in sublist]
    result = np.zeros(shape=[1,12])
    result[0] = np.array(flatObservation)
    return result

def convertObsLineToNumber(line:list):
    """
    Converts single line with characters to number
    :param line: line with max 4 characters
    :return: int number representing this line
    """
    power = 3
    total = 0
    for elem in line:
        total = total + mapToInt(elem) * pow(2, power)
        power -= 1
    return total

def convertObservationToNum(observation):
    """
    Converts observation array with characters to single integer number (max 12 binary digits)
    :param observation: array with max 3 lines , each has max 4 characters
    :return:
    """
    power = 8
    total = 0
    for line in observation:
        total = total + convertObsLineToNumber(line) * pow(2, power)
        power -= 4
    return total


def convertObservationToBinaryInputs(observation, allInputs):
    """
    Converts observation to array with size of inputsAmount
    :param observation: observation to convert as array with line and characters
    :param inputsAmount: the amount of inputs (the cells in returned array)
    :return: array with only single 1 in one of the input out of inputsAmount inputs
    """
    # first convert to number
    observationAsNum = convertObservationToNum(observation)
    # then convert to array representation with 2 pow 12 combinations:
    return allInputs [observationAsNum:observationAsNum + 1]

def mapToInt(cellValue):
    return 1 if cellValue in b'OP' else 0

def mapToFloat(cellValue):
    return 1. if cellValue in b'OP' else 0.

###############################################################
# TEST for converting to array
# ###############################################################
#
# print("Test......")
# # MAPS1 = {
# #     "3x14": [
# #         "P     O      G",
# #         "  O       O  G",
# #         "  OO         G"
# #     ]
# # }
# MAPS = {
#     "3x2": [
#         "PO",
#         " G",
#         " O"
#     ]
# }
# descMap = MAPS["3x2"]
# descArr=np.asarray(descMap, dtype='c')
# toConvert=np.array(descArr[:,0:2])
# print("To convert:")
# print(toConvert)
#
# result=convertObservation(toConvert)
#
# print("Result:")
# print(result)

##############################################################
# TEST for converting to num
##############################################################
#
# print("Test converting to Num")
# MAPS = {
#     "3x14": [
#         "OOOO",
#         "OOOO",
#         "OOO "
#     ]
# }
# #--> RESULT : [[0,0,0,0....1,0]]
# # MAPS = {
# #     "3x14": [
# #         "    ",
# #         "    ",
# #         "    "
# #     ]
# # }
# # #--> RESULT : [[1,0,0,0....0,0]]
# descMap = MAPS["3x14"]
# descArr=np.asarray(descMap, dtype='c')
# toConvert=np.array(descArr[:,0:4])
# print("To convert:")
# print(toConvert)
#
# print(convertObservationToNum(toConvert))
# result=convertObservationToBinaryInputs(toConvert, np.identity(pow(2,12)))
# print("Result as array with just 1 bit set (should be 0,0,0,0,....,0,1,0):")
# print(result)
# # TODO: assert -> sum of these bits - should be 1
# #print(sum(sum(result[:])))

###############################################################
# TEST for converting to num
###############################################################

# print("Test converting to Num")
# MAPS = {
#     "3x14": [
#         "P     O      G",
#         "  O       O  G",
#         "  OO         G"
#     ]
# }
# descMap = MAPS["3x14"]
# descArr=np.asarray(descMap, dtype='c')
# toConvert=np.array(descArr[:,0:3])
# print("To convert:")
# print(toConvert)
#
# result=convertObservation(toConvert)
# print("Result:")
# print(result)
# result=convertObservationToNum(toConvert)
# print("Result as number:")
# print(result)
# # TODO: assert -> should be 2082 considering view 3x3
