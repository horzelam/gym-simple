import matplotlib.pyplot as plt

def plot(rList):
    """
    Plots the chart for RESULTS with some SUCCESS RATE info
    :param rList: list of results in episodes (1 or 0)
    :return: None
    """
    successRatio = "Percent of succesful episodes: " + str(100 * sum(rList) / len(rList)) + "%"

    plt.ylabel('1 - WINNER, 0 - LOSER' , fontsize='xx-large')
    plt.xlabel('Episode number. \n{}'.format(successRatio ) , fontsize='xx-large')
    index = 0
    for elem in rList:
        index += 1
        if elem > 0:
            pointCol = 'b'
        else:
            pointCol = 'r'
        plt.plot(index, elem, 'ro', color=pointCol, ms=1.5)

    # OLD:
    #plt.plot(rList, 'ro', color='g')
    # plt.plot(jList, 'ro', color='r')

    plt.show()